<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>eu.melodic.bpm</groupId>
    <artifactId>process</artifactId>
    <version>3.1.0-SNAPSHOT</version>

    <properties>
        <lombok.version>1.16.16</lombok.version>
        <camunda.version>7.8.0</camunda.version>
        <maven-compiler.version>3.2</maven-compiler.version>
        <!--DOCKER plugin properties-->
        <docker.push>false</docker.push>
        <docker.repository>127.0.0.1:5000/</docker.repository>
        <docker.imagePrefix>melodic-${scmBranch}/</docker.imagePrefix>
        <docker.imageName>process</docker.imageName>
        <docker.jarToInclude>${project.build.finalName}.jar</docker.jarToInclude>
        <docker.spotify-plugin.version>0.4.10</docker.spotify-plugin.version>
        <project.scm.id>eu.7bulls</project.scm.id> <!--this is required for scm section - do not remove this -->
    </properties>

    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-dependencies</artifactId>
                <version>1.5.7.RELEASE</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <dependencies>
        <dependency>
            <groupId>eu.melodic</groupId>
            <artifactId>interfaces</artifactId>
            <version>3.1.0-SNAPSHOT</version>
        </dependency>

        <dependency>
            <groupId>org.camunda.bpm.springboot</groupId>
            <artifactId>camunda-bpm-spring-boot-starter-webapp</artifactId>
            <version>2.3.0-alpha1</version>
        </dependency>
        <dependency>
            <groupId>com.h2database</groupId>
            <artifactId>h2</artifactId>
        </dependency>

        <dependency>
            <groupId>org.camunda.bpm.springboot</groupId>
            <artifactId>camunda-bpm-spring-boot-starter-rest</artifactId>
            <version>2.3.0-alpha1</version>
        </dependency>

        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <version>${lombok.version}</version>
            <scope>provided</scope>
        </dependency>

        <dependency>
            <groupId>org.camunda.bpm.identity</groupId>
            <artifactId>camunda-identity-ldap</artifactId>
            <version>${camunda.version}</version>
        </dependency>

    </dependencies>

    <build>
        <finalName>${project.artifactId}</finalName>

        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>${maven-compiler.version}</version>
                <inherited>true</inherited>
                <configuration>
                    <source>1.8</source>
                    <target>1.8</target>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
                <configuration>
                    <layout>ZIP</layout>
                </configuration>
                <executions>
                    <execution>
                        <goals>
                            <goal>repackage</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>com.spotify</groupId>
                <artifactId>docker-maven-plugin</artifactId>
                <version>${docker.spotify-plugin.version}</version>
                <configuration>
                    <dockerDirectory>${project.basedir}/src/main/docker</dockerDirectory>
                    <imageName>${docker.repository}${docker.imagePrefix}${docker.imageName}</imageName>
                    <pushImage>${docker.push}</pushImage>
                    <resources>
                        <resource>
                            <targetPath>/</targetPath>
                            <directory>${project.build.directory}</directory>
                            <include>${docker.jarToInclude}</include>
                        </resource>
                    </resources>
                </configuration>
                <executions>
                    <execution>
                        <id>execution</id>
                        <phase>install</phase>
                        <goals>
                            <goal>build</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <!--plugins for getting scmBranch value lowercase to use in docker images -->
            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>buildnumber-maven-plugin</artifactId>
                <version>1.4</version>
                <executions>
                    <execution>
                        <phase>validate</phase>
                        <goals>
                            <goal>create</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.codehaus.groovy.maven</groupId>
                <artifactId>gmaven-plugin</artifactId>
                <version>1.0</version>
                <executions>
                    <execution>
                        <phase>initialize</phase>
                        <goals>
                            <goal>execute</goal>
                        </goals>
                        <configuration>
                            <source>
                                import org.apache.commons.lang.StringUtils

                                project.properties["scmBranch"] = StringUtils.lowerCase(project.properties["scmBranch"])
                            </source>
                        </configuration>
                    </execution>
                </executions>
            </plugin>

        </plugins>

    </build>

    <profiles>
        <profile>
            <id>without-docker</id>
            <build>
                <plugins>
                    <plugin>
                        <groupId>com.spotify</groupId>
                        <artifactId>docker-maven-plugin</artifactId>
                        <executions>
                            <execution>
                                <id>execution</id>
                                <phase>none</phase>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>
            </build>
        </profile>

        <profile>
            <id>docker-remote</id>
            <properties>
                <docker.repository>88.99.85.63:5000/</docker.repository>
                <docker.push>true</docker.push>
            </properties>
        </profile>

    </profiles>

    <repositories>
        <repository>
            <id>eu.7bulls</id>
            <name>Melodic 7bulls repository</name>
            <url>https://nexus.7bulls.eu:8443/repository/maven-public/</url>
            <releases>
                <enabled>true</enabled>
            </releases>
            <snapshots>
                <enabled>true</enabled>
            </snapshots>
        </repository>
    </repositories>

    <distributionManagement>
        <snapshotRepository>
            <id>eu.7bulls</id>
            <name>Melodic 7bulls repository</name>
            <url>https://nexus.7bulls.eu:8443/repository/maven-snapshots/</url>
        </snapshotRepository>
        <repository>
            <id>eu.7bulls</id>
            <name>Melodic 7bulls repository</name>
            <url>https://nexus.7bulls.eu:8443/repository/maven-releases/</url>
        </repository>
    </distributionManagement>

    <scm>
        <connection>scm:git:https://bitbucket.7bulls.eu/scm/mel/integration.git</connection>
        <developerConnection>scm:git:https://bitbucket.7bulls.eu/scm/mel/integration.git</developerConnection>
    </scm>

</project>
