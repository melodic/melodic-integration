/*
 * Copyright (C) 2017 7bulls.com
 *
 * This Source Code Form is subject to the terms of the
 * Mozilla Public License, v. 2.0. If a copy of the MPL
 * was not distributed with this file, You can obtain one at
 * http://mozilla.org/MPL/2.0/.
 */

package eu.melodic.bpm.services;

import eu.melodic.bpm.exception.JwtExpiredException;
import eu.melodic.models.commons.NotificationResult;
import eu.melodic.models.commons.NotificationResultImpl;
import eu.melodic.models.commons.Watermark;
import eu.melodic.models.commons.WatermarkImpl;
import eu.melodic.models.services.process.*;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Slf4j
@Service
@AllArgsConstructor(onConstructor = @__({@Autowired}))
public class ApplicationDeploymentExecutor {

  private Environment env;
  private RestTemplate restTemplate;

  private static final String ACCESS_TOKEN_VARIABLE = "melodicAuthorizationToken";
  private static final String REFRESH_TOKEN_VARIABLE = "refreshToken";
  private static final String APPLICATION_ID_VARIABLE= "applicationId";


  //sends create constraint problem request to the esb
  public void processConstraintProblem(DelegateExecution execution) {

    if(!verifyIfProcessVariablesSet(execution)) {
      log.error("No mandatory variables present in the process!");
      return;
    }
    log.info("Preparing ConstraintProblemRequest...");
    ConstraintProblemRequest request = prepareConstraintProblemRequest(execution.getVariable(APPLICATION_ID_VARIABLE).toString(), execution.getVariable("businessKey").toString());
    log.info("ConstraintProblemRequest prepared. Sending...");
    sendAuthorizedPOSTRequest(execution, request, "constraintProblem", execution.getVariable(ACCESS_TOKEN_VARIABLE).toString());
    log.info("ConstraintProblemRequest sent.");
  }

  public void processDataModel(DelegateExecution execution) {

    if(!verifyIfProcessVariablesSet(execution)) {
      log.error("No mandatory variables present in the process!");
      return;
    }
    log.info("Preparing DataModelRequest...");
    DataModelRequest request = prepareDataModelRequest(execution.getVariable(APPLICATION_ID_VARIABLE).toString(), execution.getVariable("businessKey").toString());
    log.info("DataModelRequest prepared. Sending...");
    sendAuthorizedPOSTRequest(execution, request, "dataModel", execution.getVariable(ACCESS_TOKEN_VARIABLE).toString());
    log.info("DataModelRequest sent.");
  }

  public void processEventModel(DelegateExecution execution) {

    if(!verifyIfProcessVariablesSet(execution)) {
      log.error("No mandatory variables present in the process!");
      return;
    }
    log.info("Preparing EventModelRequest...");
    EventModelRequest request = prepareEventModelRequest(execution.getVariable(APPLICATION_ID_VARIABLE).toString(), execution.getVariable("businessKey").toString());
    log.info("EventModelRequest prepared. Sending...");

    sendAuthorizedPOSTRequest(execution, request, "eventModel", execution.getVariable(ACCESS_TOKEN_VARIABLE).toString());
    log.info("EventModelRequest sent.");
  }

    public void processConstraintProblemEnhancement(DelegateExecution execution) {

    if(!verifyIfProcessVariablesSet(execution)) {
      log.error("No mandatory variables present in the process!");
      return;
    }
    log.info("Preparing ConstraintProblemEnhancementRequest...");
    ConstraintProblemEnhancementRequest request = prepareConstraintProblemEnhancementRequest(execution.getVariable(APPLICATION_ID_VARIABLE).toString(), execution.getVariable("businessKey").toString(), execution.getVariable("cpCdoPath").toString());
    log.info("ConstraintProblemEnhancementRequest prepared. Sending...");

    ConstraintProblemEnhancementResponseImpl response = sendAuthorizedPOSTRequestWithResponse(execution, request, "constraintProblemEnhancement", ConstraintProblemEnhancementResponseImpl.class, execution.getVariable(ACCESS_TOKEN_VARIABLE).toString());
    log.info("ConstraintProblemRequest sent.");
    if(response.getResult().getStatus() !=null &&  response.getResult().getStatus() == NotificationResult.StatusType.SUCCESS){
      if(response.getDesignatedSolver() != null){
        execution.setVariable("designatedSolver", response.getDesignatedSolver().toString() );
      }
    }
  }

  private Boolean verifyIfProcessVariablesSet(DelegateExecution execution){
    //    Check if all mandatory variables are set
    if (execution.getVariable("businessKey") == null) {
      execution.setVariable("businessKey", execution.getProcessInstanceId());
    }
    if (execution.getVariable(APPLICATION_ID_VARIABLE) == null) {
      log.error("No applicationId present in the process!");
      return false;
    }
    if (execution.getVariable(ACCESS_TOKEN_VARIABLE) == null) {
      log.warn("No melodicAuthorizationToken present in the process.");
      execution.setVariable(ACCESS_TOKEN_VARIABLE, "");
    }
    if(execution.getVariable(REFRESH_TOKEN_VARIABLE) == null){
      log.warn("No refreshToken present in the process.");
      execution.setVariable(REFRESH_TOKEN_VARIABLE, "");
    }
    return true;
  }

  public void processSolutionEvaluation(DelegateExecution execution) {

    if(!verifyIfProcessVariablesSet(execution)) {
      log.error("No mandatory variables present in the process!");
      return;
    }

    log.info("Preparing solutionEvaluation Request...");
    SolutionEvaluationRequest request = prepareSolutionEvaluationtRequest(execution.getVariable(APPLICATION_ID_VARIABLE).toString(), execution.getVariable("businessKey").toString(), execution.getVariable("cpCdoPath").toString());
    log.info("solutionEvaluation request prepared. Sending...");

    SolutionEvaluationResponseImpl response = sendAuthorizedPOSTRequestWithResponse(execution, request, "solutionEvaluation", SolutionEvaluationResponseImpl.class,execution.getVariable(ACCESS_TOKEN_VARIABLE).toString());
    log.info("solutionEvaluation sent.");
    if(response.getEvaluationResult() !=null){
        execution.setVariable("solutionEvaluationResult", response.getEvaluationResult().toString() );
    }
  }

  public void processDiscoveryStatus(DelegateExecution execution) {

    Boolean dsFinished=false;
    Integer dsRetries = Integer.valueOf(env.getProperty("discovery.retries"));
    Integer dsDelay = Integer.valueOf(env.getProperty("discovery.delay"));

    execution.setVariable("discoveryServiceResult", "FAILURE" );

    int i=0;
    while (!dsFinished && i<dsRetries ){
      i++;
      log.debug("Sending discovery status request #"+i);
      DiscoveryStatusResponseImpl response = sendGETRequestWithResponse("discoveryStatus", DiscoveryStatusResponseImpl.class);

      if(response.getTotal()>0 && response.getHardwareDiscoveryWorker()==0 && response.getImageDiscoveryWorker()==0 && response.getLocationDiscoveryWorker()==0  ){
        log.info("Discovery service finished. Total: "+response.getTotal()+", HW: "+response.getHardwareDiscoveryWorker()+", IM: "+response.getImageDiscoveryWorker()+", LC: "+response.getLocationDiscoveryWorker());
        dsFinished=true;
        execution.setVariable("discoveryServiceResult", "SUCCESS" );
      } else {
        log.info("Discovery service not yet finished. Try #"+i+": total: "+response.getTotal()+", HW: "+response.getHardwareDiscoveryWorker()+", IM: "+response.getImageDiscoveryWorker()+", LC: "+response.getLocationDiscoveryWorker());
        try {
          TimeUnit.SECONDS.sleep(dsDelay);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    }
  }

  public void processSolutionUpdate(DelegateExecution execution) {

    if(!verifyIfProcessVariablesSet(execution)) {
      log.error("No mandatory variables present in the process!");
      return;
    }

    log.info("Preparing solutionUpdate Request...");
    SolutionUpdateRequest request = prepareSolutionUpdateRequest(execution.getVariable(APPLICATION_ID_VARIABLE).toString(), execution.getVariable("businessKey").toString(), execution.getVariable("cpCdoPath").toString(), execution.getVariable("applicationDeploymentResultCode").toString());
    log.info("solutionUpdate request prepared. Sending...");

    SolutionUpdateResponseImpl response = sendAuthorizedPOSTRequestWithResponse(execution, request, "updateSolution", SolutionUpdateResponseImpl.class, execution.getVariable(ACCESS_TOKEN_VARIABLE).toString());
    log.info("solutionUpdate sent.");
    if(response.getUpdateResult() !=null){
      log.info("Setting solutionUpdateResult to: " +response.getUpdateResult().getStatus().toString());
      execution.setVariable("solutionUpdateResult", response.getUpdateResult().getStatus().toString() );
    }
      execution.setVariable("deployedSolutionId", response.getDeployedSolutionId() );
  }


  //sends create constraint problem solution request to the esb
  public void processConstraintProblemSolution(DelegateExecution execution) {

    if(!verifyIfProcessVariablesSet(execution)) {
      log.error("No mandatory variables present in the process!");
      return;
    }

    log.info("Preparing ConstraintProblemSolutioRequest for application id: " +execution.getVariable(APPLICATION_ID_VARIABLE).toString() +" , cpModelPath: "+execution.getVariable("cpCdoPath").toString());
    String desingatedSolver="";
    if (execution.getVariable("designatedSolver") != null){
      desingatedSolver =(String)execution.getVariable("designatedSolver");
    }
    else {
      log.warn("Designated Solver was not defined, using CP Solver.");
      desingatedSolver = SolutionRequest.DesignatedSolverType.CPSOLVER.name();
    }
    log.info("Designated Solver: "+ desingatedSolver);
    SolutionRequest request = prepareSolutionRequest(execution.getVariable(APPLICATION_ID_VARIABLE).toString(), execution.getVariable("cpCdoPath").toString(), execution.getVariable("businessKey").toString(), desingatedSolver);
    log.info("ConstraintProblemSolutionRequest prepared. Sending...");
    sendAuthorizedPOSTRequest(execution, request, "solution", execution.getVariable(ACCESS_TOKEN_VARIABLE).toString());
    log.info("ConstraintProblemSolutionRequest sent.");
  }

  //sends ApplySolutionRequest request to the esb
  public void processApplySolution(DelegateExecution execution) {

    if(!verifyIfProcessVariablesSet(execution) || execution.getVariable("cpCdoPath")==null) {
      log.error("No mandatory variables present in the process!");
      return;
    }

    log.info("Preparing ApplySolutionRequest...");
    ApplySolutionRequest request = prepareApplySolutionRequest(execution.getVariable(APPLICATION_ID_VARIABLE).toString(), execution.getVariable("cpCdoPath").toString(), execution.getVariable("businessKey").toString());
    log.info("ApplySolutionRequest prepared. Sending...");
    sendAuthorizedPOSTRequest(execution, request, "applySolution", execution.getVariable(ACCESS_TOKEN_VARIABLE).toString());
    log.info("ApplySolutionRequest sent.");
  }

  //sends create ApplicationDeploymentRequest request to the esb
  public void processApplicationDeployment(DelegateExecution execution) {

    if(!verifyIfProcessVariablesSet(execution)) {
      log.error("No mandatory variables present in the process!");
      return;
    }

    log.info("Preparing ApplicationDeploymentRequest...");
    ApplicationDeploymentRequest request = prepareApplicationDeploymentRequest(execution.getVariable(APPLICATION_ID_VARIABLE).toString(),
            execution.getVariable("businessKey").toString(), execution.getVariable("cpCdoPath").toString(), execution.getVariable("isSimulation").toString());
    log.info("ApplicationDeploymentRequest prepared. Sending...");
    sendAuthorizedPOSTRequest(execution, request, "applicationDeployment", execution.getVariable(ACCESS_TOKEN_VARIABLE).toString());
    log.info("ApplicationDeploymentRequest sent.");
  }

  private void sendAuthorizedPOSTRequest(DelegateExecution execution, Object request, String resourceURI, String authorizationToken){
      if (authorizationToken!=null && !("".equals(authorizationToken))) {
          MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
          headers.add("Authorization", authorizationToken);
          HttpEntity<Object> requestWithHeaders = new HttpEntity<>(request, headers);
          log.info("Sending authorized request with token: " +authorizationToken);

          try {
              sendPOSTRequest(requestWithHeaders, resourceURI);
              log.debug("Request sent.");
          }
          catch (JwtExpiredException exception){
              log.info("Access token {} expired, refreshing token...", authorizationToken);
              processRefreshToken(execution);
              sendAuthorizedPOSTRequest(execution, request, resourceURI, execution.getVariable(ACCESS_TOKEN_VARIABLE).toString());
              log.info("Request successfully retried");
          }
      } else {
          log.warn("No authorization token present in the process! Sending NON-authorized request!");
          sendPOSTRequest(request, resourceURI);
      }

  }

  private void sendPOSTRequest(Object request, String resourceURI) throws JwtExpiredException{
    String esbUrl = createEsbUrl(resourceURI);

    try {
      log.debug("Sending POST request to: "+esbUrl);
      restTemplate.postForEntity(esbUrl, request, String.class);
      log.debug("Request sent.");
    }
    catch (RestClientResponseException restResponseException){
      log.error("Error sending request: {}, status code: {}", restResponseException.getMessage(), restResponseException.getRawStatusCode());
      int statusCode = restResponseException.getRawStatusCode();
      if (statusCode == 403){
        throw new JwtExpiredException();
      }
    }
    catch (RestClientException restException){
      log.error("Error sending request: " + restException.getMessage());
    }
  }

  private <T> T sendAuthorizedPOSTRequestWithResponse(DelegateExecution execution, Object request, String resourceURI, Class<T> responseType, String authorizationToken) throws RestClientException {

    if(authorizationToken!=null) {
      MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
      headers.add("Authorization", authorizationToken);
      HttpEntity<Object> requestWithHeaders = new HttpEntity<Object>(request, headers);
      log.debug("Sending authorized request with token: " +authorizationToken);
      T response;
      try {
        response = sendPOSTRequestWithResponse(requestWithHeaders, resourceURI, responseType);
        return response;
      } catch (JwtExpiredException exception){
        log.info("Access token expired, refreshing token...", authorizationToken);
        processRefreshToken(execution);
        return sendAuthorizedPOSTRequestWithResponse(execution, request, resourceURI, responseType, execution.getVariable(ACCESS_TOKEN_VARIABLE).toString());
      }
    } else {
      log.warn("No authorization token present in the process! Sending NON-authorized request!");
      return sendPOSTRequestWithResponse(request, resourceURI, responseType);
    }
  }

  private <T> T sendPOSTRequestWithResponse(Object request, String resourceURI, Class<T> responseType) throws RestClientException, JwtExpiredException {

    String esbUrl = createEsbUrl(resourceURI);
    log.debug("Sending POST request to: "+esbUrl);
    ResponseEntity<T> responseEntity;
    try {
      responseEntity = restTemplate.postForEntity(esbUrl, request, responseType);
      log.debug("Request sent.");
    }
    catch (RestClientResponseException restResponseException){
      log.error("Error sending request: {}, status code: {}", restResponseException.getMessage(), restResponseException.getRawStatusCode());
      if (restResponseException.getRawStatusCode() == 403) {
        throw new JwtExpiredException();
      }
      else {
          throw restResponseException;
      }
    }
    return responseEntity.getBody();
  }

  private <T> T sendGETRequestWithResponse(String resourceURI, Class<T> responseType) throws RestClientException {

    String esbUrl = createEsbUrl(resourceURI);
    log.debug("Sending GET request to: "+esbUrl);
    ResponseEntity<T> responseEntity = restTemplate.getForEntity(esbUrl, responseType);
    log.debug("Request sent.");
    return responseEntity.getBody();
  }

  private Watermark prepareWatermark(String uuid) {
    Watermark watermark = new WatermarkImpl();
    watermark.setUser("Camunda");
    watermark.setSystem("Camunda");
    watermark.setDate(new Date());
    watermark.setUuid(uuid);
    return watermark;
  }

  private NotificationResult prepareNotificationResult(String returnCode) {

    NotificationResult result = new NotificationResultImpl();

    if(returnCode.equalsIgnoreCase(NotificationResult.StatusType.SUCCESS.toString())){
      result.setStatus(NotificationResult.StatusType.SUCCESS);
    }
    else {
      result.setStatus(NotificationResult.StatusType.ERROR);
    }
    return result;
  }

  private ConstraintProblemRequest prepareConstraintProblemRequest(String applicationId, String uuid) {
    ConstraintProblemRequest request = new ConstraintProblemRequestImpl();
    request.setApplicationId(applicationId);
    request.setNotificationSubject("ConstraintProblemNotification");
    request.setProcessId(uuid);
    request.setWatermark(prepareWatermark(uuid));
    return request;
  }

  private DataModelRequest prepareDataModelRequest(String applicationId, String uuid) {
    DataModelRequest request = new DataModelRequestImpl();
    request.setApplicationId(applicationId);
    request.setNotificationSubject("DataModelNotification");
    request.setProcessId(uuid);
    request.setWatermark(prepareWatermark(uuid));
    return request;
  }

  private EventModelRequest prepareEventModelRequest(String applicationId, String uuid) {
    EventModelRequest request = new EventModelRequestImpl();
    request.setApplicationId(applicationId);
    request.setNotificationSubject("EventModelNotification");
    request.setProcessId(uuid);
    request.setWatermark(prepareWatermark(uuid));
    return request;
  }

  private ConstraintProblemEnhancementRequest prepareConstraintProblemEnhancementRequest(String applicationId, String uuid, String cpCdoPath) {
    ConstraintProblemEnhancementRequest request = new ConstraintProblemEnhancementRequestImpl();
    request.setApplicationId(applicationId);
    request.setCdoModelsPath(cpCdoPath);
    request.setWatermark(prepareWatermark(uuid));
    return request;
  }

  private SolutionEvaluationRequest prepareSolutionEvaluationtRequest(String applicationId, String uuid, String cpCdoPath) {
    SolutionEvaluationRequest request = new SolutionEvaluationRequestImpl();
    request.setApplicationId(applicationId);
    request.setCdoModelsPath(cpCdoPath);
    request.setWatermark(prepareWatermark(uuid));
    return request;
  }

  private SolutionUpdateRequest prepareSolutionUpdateRequest(String applicationId, String uuid, String cpCdoPath, String applicationDeploymentResultCode) {
    SolutionUpdateRequest request = new SolutionUpdateRequestImpl();
    request.setApplicationId(applicationId);
    request.setCdoModelsPath(cpCdoPath);
    request.setDeploymentResult(prepareNotificationResult(applicationDeploymentResultCode));
    request.setWatermark(prepareWatermark(uuid));
    return request;
  }

  private SolutionRequest prepareSolutionRequest(String applicationId, String cdoResourcePath, String uuid, String designatedSolver ) {
    SolutionRequest request = new SolutionRequestImpl();
    request.setCdoResourcePath(cdoResourcePath);
    request.setApplicationId(applicationId);
    request.setNotificationSubject("ConstraintProblemSolutionNotification");
    request.setProcessId(uuid);
    request.setDesignatedSolver(SolutionRequest.DesignatedSolverType.valueOf(designatedSolver));
    request.setWatermark(prepareWatermark(uuid));
    return request;
  }

  private ApplySolutionRequest prepareApplySolutionRequest(String applicationId, String cdoResourcePath, String uuid ) {
    ApplySolutionRequest request = new ApplySolutionRequestImpl();
    request.setCdoResourcePath(cdoResourcePath);
    request.setApplicationId(applicationId);
    request.setNotificationSubject("ApplySolutionNotification");
    request.setProcessId(uuid);
    request.setWatermark(prepareWatermark(uuid));
    return request;
  }

  private ApplicationDeploymentRequest prepareApplicationDeploymentRequest(String applicationId,  String uuid,
                                                                           String cdoResourcePath, String isSimulation) {
    ApplicationDeploymentRequest request = new ApplicationDeploymentRequestImpl();
    request.setApplicationId(applicationId);
    request.setCdoResourcePath(cdoResourcePath);
    request.setNotificationSubject("ApplicationDeploymentNotification");
    request.setProcessId(uuid);
    request.setIsSimulation(isSimulation);
    request.setWatermark(prepareWatermark(uuid));
    return request;
  }



  private void processRefreshToken(DelegateExecution execution) {

    if(!verifyIfProcessVariablesSet(execution)) {
      log.error("No mandatory variables present in the process!");
      return;
    }
    log.info("Preparing RefreshTokenRequest...");
    String refreshToken = execution.getVariable(REFRESH_TOKEN_VARIABLE).toString();
    if (refreshToken!=null && !("".equals(refreshToken))) {

      HttpHeaders headers = new HttpHeaders();
      headers.set("Authorization", refreshToken);
      HttpEntity entity = new HttpEntity(headers);

      log.info("Sending refresh Token request with token: " + refreshToken);

      String esbUrl = createEsbUrl("refreshToken");
      log.info("Sending GET request to: " + esbUrl);
      ResponseEntity<String> responseEntity = restTemplate.exchange(esbUrl, HttpMethod.GET, entity, String.class);
      HttpHeaders responseEntityHeaders = responseEntity.getHeaders();
      log.debug("getHeaders: {}", responseEntityHeaders);

      List<String> authorization = responseEntityHeaders.get("Authorization");
      List<String> refresh = responseEntityHeaders.get("refresh");
      execution.setVariable(REFRESH_TOKEN_VARIABLE, refresh.get(0));
      execution.setVariable(ACCESS_TOKEN_VARIABLE, authorization.get(0));
      log.info("Tokens successfully refreshed.");

    }
    else {
      log.error("RefreshToken not set, cannot refresh Token");
    }
  }

  private String createEsbUrl(String resourceURI){
      String esbUrl = env.getProperty("esb.url");
      if (esbUrl.endsWith("/")) {
          esbUrl = esbUrl.substring(0, esbUrl.length() - 1);
      }
      esbUrl=esbUrl+"/"+resourceURI;
      return esbUrl;
  }

}
