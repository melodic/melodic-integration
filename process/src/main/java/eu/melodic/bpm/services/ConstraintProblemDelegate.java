package eu.melodic.bpm.services;


import java.util.UUID;
import java.util.logging.Logger;

import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

/**
 * Created by mprusinski on 28.10.17.
 */

//@AllArgsConstructor(onConstructor = @__({@Autowired}))
@Slf4j
public class ConstraintProblemDelegate implements JavaDelegate {

  @Autowired
  Environment env;

  @Autowired
  ApplicationDeploymentExecutor executor;

  //private final static Logger LOGGER = Logger.getLogger("MELODIC_PROCESS");

  public void execute(DelegateExecution execution) throws Exception {

    if (execution.getVariable("businessKey") == null) {
      execution.setVariable("businessKey", execution.getProcessInstanceId());
    }

    log.info("Processing Constraint Problem for process: "+execution.getProcessInstanceId());
    executor.processConstraintProblem(execution);
    log.info("Problem created for process: "+execution.getProcessInstanceId());

  }

}

