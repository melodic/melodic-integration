package eu.melodic.bpm.services;


import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import java.util.Calendar;

/**
 * Created by mprusinski on 28.10.17.
 */

//@AllArgsConstructor(onConstructor = @__({@Autowired}))
@Slf4j
public class StartProcessVerifierDelegate implements JavaDelegate {

    @Autowired
    Environment env;

    @Autowired
    ApplicationDeploymentExecutor executor;

    public void execute(DelegateExecution execution) throws Exception {

        execution.setVariableLocal("localStartProcessDecision","START");

        if (execution.getVariable("businessKey") == null) {
            execution.setVariable("businessKey", execution.getProcessInstanceId());
        }

        if (execution.getVariable("applicationId") == null) {
            execution.setVariable("localStartProcessDecision", "STOP");
            return;
        }

        //set process variables according to properties
        log.info("Setting variable dataManagementEnabled to: "+Boolean.parseBoolean(env.getProperty("dataManagement.enabled")));
        execution.setVariable("dataManagementEnabled", Boolean.parseBoolean(env.getProperty("dataManagement.enabled")));

        log.info("Setting variable eventManagementEnabled to: "+Boolean.parseBoolean(env.getProperty("eventManagement.enabled")));
        execution.setVariable("eventManagementEnabled", Boolean.parseBoolean(env.getProperty("eventManagement.enabled")));

        //check if there is already a process for the same application running
        log.info("Checking if there is already running deployment process for application: "+execution.getVariable("applicationId"));
        ProcessInstance pi = execution.getProcessEngineServices()
                .getRuntimeService()
                .createProcessInstanceQuery()
                .variableValueEquals("applicationId", execution.getVariable("applicationId"))
                .variableValueNotEquals("processState","FINISHED")
                .singleResult();

        if (pi !=null){
            log.info("There is already running process with ID: " +pi.getProcessInstanceId().toString() + " for application: "+execution.getVariable("applicationId") +". This process shall NOT start.");
            execution.setVariable("localStartProcessDecision", "STOP");
        } else if (!Boolean.parseBoolean(execution.getVariable("isSimulation").toString())){
            //we don't want to wait if application is launched in simulation mode
            log.info("No other running processes for application: "+execution.getVariable("applicationId") + ", check if enough time passed since last process finished.");

            Calendar calendar = Calendar.getInstance();

            Integer delay =  Integer.parseInt(env.getProperty("deployment.delay"));
            if(delay == null){
                delay=0;
            }
            calendar.add(Calendar.SECOND, -delay);

            log.debug("Checking if: " +calendar.getTime().toString() + " is greater than last processFinishTime.");

            pi = execution.getProcessEngineServices()
                    .getRuntimeService()
                    .createProcessInstanceQuery()
                    .variableValueEquals("applicationId", execution.getVariable("applicationId"))
                    .variableValueEquals("processState","FINISHED")
                    .variableValueGreaterThan("processFinishDate", calendar.getTime())
                    .singleResult();

            if (pi !=null) {
                log.info("Not enough time has passed since last execution of the process: " + pi.getProcessInstanceId().toString() + ". Rejecting process...");
                execution.setVariable("localStartProcessDecision", "STOP");
            }

        }
        return;
    }
}
