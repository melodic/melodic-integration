package eu.melodic.bpm.services;


import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

/**
 * Created by mprusinski on 28.10.17.
 */

//@AllArgsConstructor(onConstructor = @__({@Autowired}))
@Slf4j
public class ConstraintProblemEnhancementDelegate implements JavaDelegate {

  @Autowired
  Environment env;

  @Autowired
  ApplicationDeploymentExecutor executor;

  //private final static Logger LOGGER = Logger.getLogger("MELODIC_PROCESS");

  public void execute(DelegateExecution execution) throws Exception {

    log.info("Processing Constraint Problem Enhancement for process: "+execution.getProcessInstanceId());
    executor.processConstraintProblemEnhancement(execution);

    log.info("Problem enhanced for process: "+execution.getProcessInstanceId());

  }

}

