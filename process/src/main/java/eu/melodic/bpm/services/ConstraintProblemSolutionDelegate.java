package eu.melodic.bpm.services;


import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import java.util.logging.Logger;

/**
 * Created by mprusinski on 28.10.17.
 */

//@AllArgsConstructor(onConstructor = @__({@Autowired}))
@Slf4j
public class ConstraintProblemSolutionDelegate implements JavaDelegate {

  @Autowired
  Environment env;

  @Autowired
  ApplicationDeploymentExecutor executor;

  //private final static Logger LOGGER = Logger.getLogger("MELODIC_PROCESS");

  public void execute(DelegateExecution execution) throws Exception {

    log.info("Processing Constraint Problem Solution for process: "+execution.getProcessInstanceId());
    executor.processConstraintProblemSolution(execution);
    log.info("Constraint Problem Solution created for process: "+execution.getProcessInstanceId());

  }

}

