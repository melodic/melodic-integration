package eu.melodic.bpm;

import org.camunda.bpm.application.ProcessApplication;
import org.camunda.bpm.application.impl.ServletProcessApplication;

/**
 * Created by mprusinski on 28.10.17.
 */
@ProcessApplication("Process deployment app")
public class DeploymentProcess extends ServletProcessApplication{

}
