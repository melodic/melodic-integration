package eu.melodic.bpm;

import eu.melodic.bpm.services.*;
import lombok.AllArgsConstructor;
import org.camunda.bpm.identity.impl.ldap.plugin.LdapIdentityProviderPlugin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;


/**
 * Created by mprusinski on 29.10.17.
 */
@Configuration
@PropertySource("file:${MELODIC_CONFIG_DIR}/eu.melodic.upperware.security.properties")
@AllArgsConstructor(onConstructor = @__({@Autowired}))
public class ApplicationContext {

    private Environment env;

  @Bean
  public RestTemplate getRestTemplate() {
    return new RestTemplate();
  }

  @Bean
  public ConstraintProblemDelegate getConstraintProblemDelegate() {
    return new ConstraintProblemDelegate();
  }
  @Bean
  public ConstraintProblemSolutionDelegate getConstraintProblemSolutionDelegate() {
    return new ConstraintProblemSolutionDelegate();
  }
  @Bean
  public ApplySolutionDelegate getApplySolutionDelegate() {
    return new ApplySolutionDelegate();
  }
  @Bean
  public ApplicationDeploymentDelegate getApplicationDeploymentDelegate() {
    return new ApplicationDeploymentDelegate();
  }

  @Bean
  public ConstraintProblemEnhancementDelegate getConstraintProblemEnhancementDelegate() {
      return new ConstraintProblemEnhancementDelegate();
  }

  @Bean
  public DataModelDelegate getDataModelDelegate() {
    return new DataModelDelegate();
  }

  @Bean
  public EventModelDelegate getEventModelDelegate() {
    return new EventModelDelegate();
  }

  @Bean
  public SolutionEvaluationDelegate getSolutionEvaluationDelegate() {
    return new SolutionEvaluationDelegate();
  }

  @Bean
  public SolutionUpdateDelegate getSolutionUpdateDelegate() {
        return new SolutionUpdateDelegate();
    }

  @Bean
  public StartProcessVerifierDelegate getStartProcessVerifierDelegate() {
    return new StartProcessVerifierDelegate();
  }

  @Bean
  public DiscoveryStatusDelegate getDiscoveryStatusDelegate() {
    return new DiscoveryStatusDelegate();
  }

  @Bean LdapIdentityProviderPlugin getLdapIdentityProviderPlugin(){
      LdapIdentityProviderPlugin ldapPlugin = new LdapIdentityProviderPlugin();

      ldapPlugin.setServerUrl(env.getRequiredProperty("ldap.url"));
      ldapPlugin.setManagerDn(env.getRequiredProperty("ldap.principal"));
      ldapPlugin.setManagerPassword(env.getRequiredProperty("ldap.password"));
      ldapPlugin.setBaseDn(env.getRequiredProperty("ldap.partitionSuffix"));

      ldapPlugin.setUserSearchBase("");
      ldapPlugin.setUserSearchFilter("(objectclass=person)");
      ldapPlugin.setUserIdAttribute("sn");
      ldapPlugin.setUserFirstnameAttribute("cn");
      ldapPlugin.setUserLastnameAttribute("sn");
      ldapPlugin.setUserEmailAttribute("mail");
      ldapPlugin.setUserPasswordAttribute("userPassword");


      ldapPlugin.setAuthorizationCheckEnabled(true);

      return ldapPlugin;
  }

    @Bean
    public FilterRegistrationBean processCorsFilter() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials(true);
        config.addAllowedOrigin("*");
        config.addAllowedHeader("*");
        config.addAllowedMethod("*");
        source.registerCorsConfiguration("/**", config);

        FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean(new CorsFilter(source));
        filterRegistrationBean.setOrder(0);
        return filterRegistrationBean;
    }
}


