package transformers.cpGenerator;

import org.mule.api.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

@ContainsTransformerMethods // since Mule 3.0.1
public class CpGeneratorTransformer {
	
	//private static final Logger logger =  LoggerFactory.getLogger(CpGeneratorTransformer.class);
	
	@Transformer
	public eu.melodic.models.interfaces.cpGenerator.ConstraintProblemRequestImpl toConstraintProblemRequestImpl( eu.melodic.models.services.process.ConstraintProblemRequestImpl req)  {
		eu.melodic.models.interfaces.cpGenerator.ConstraintProblemRequestImpl serviceCP= new eu.melodic.models.interfaces.cpGenerator.ConstraintProblemRequestImpl();
		serviceCP.setApplicationId(req.getApplicationId());
		serviceCP.setNotificationURI("/api/generator/constraintProblemNotification/" + req.getProcessId() +"/" + req.getNotificationSubject());
		serviceCP.setWatermark(req.getWatermark());
		
		return serviceCP;			
	}

}
