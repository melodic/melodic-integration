package transformers.camunda;


import org.mule.api.annotations.*;
import org.mule.api.annotations.param.Payload;
import org.camunda.bpm.engine.rest.dto.runtime.ProcessInstanceDto;

import eu.melodic.models.commons.NotificationResultImpl;
import eu.melodic.models.commons.NotificationResult.StatusType;
import eu.melodic.models.services.frontend.DeploymentProcessResponseImpl;
import eu.melodic.models.services.frontend.DeploymentProcessRequestImpl;


@ContainsTransformerMethods // since Mule 3.0.1
public class CamundaTransformer {
	
	@Transformer
	public DeploymentProcessResponseImpl toDeploymentProcessResponse(ProcessInstanceDto camundaDTO)  {
		DeploymentProcessResponseImpl response= new DeploymentProcessResponseImpl();
		response.setProcessId(String.valueOf(camundaDTO.getId()));
		
		NotificationResultImpl result = new NotificationResultImpl();
		result.setStatus(StatusType.SUCCESS);
		response.setResult(result);
		
		return response;			
	}
	
	
	@Transformer
	public eu.melodic.models.services.metaSolver.DeploymentProcessResponseImpl toMetaSolverDeploymentProcessResponse(ProcessInstanceDto camundaDTO)  {
		eu.melodic.models.services.metaSolver.DeploymentProcessResponseImpl response= new eu.melodic.models.services.metaSolver.DeploymentProcessResponseImpl();
		response.setProcessId(String.valueOf(camundaDTO.getId()));
		
		NotificationResultImpl result = new NotificationResultImpl();
		result.setStatus(StatusType.SUCCESS);
		response.setResult(result);
		
		return response;			
	}
	
	
	

}
