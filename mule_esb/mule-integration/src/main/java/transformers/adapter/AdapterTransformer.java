package transformers.adapter;

import org.mule.api.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

@ContainsTransformerMethods // since Mule 3.0.1
public class AdapterTransformer {
	
	@Transformer
	public eu.melodic.models.interfaces.adapter.ApplicationDeploymentRequestImpl toApplicationDeploymentRequestImpl( eu.melodic.models.services.process.ApplicationDeploymentRequest req)  {
		eu.melodic.models.interfaces.adapter.ApplicationDeploymentRequestImpl interfaceRequest= new eu.melodic.models.interfaces.adapter.ApplicationDeploymentRequestImpl();
		interfaceRequest.setApplicationId(req.getApplicationId());
		interfaceRequest.setCdoModelsPath(req.getCdoResourcePath());
		interfaceRequest.setIsSimulation(req.getIsSimulation());
		interfaceRequest.setNotificationURI("/api/adapter/deploymentNotification/" + req.getProcessId() +"/" + req.getNotificationSubject());
		interfaceRequest.setWatermark(req.getWatermark());
		
		return interfaceRequest;			
	}

	@Transformer
	public eu.melodic.models.interfaces.adapter.ApplySolutionRequestImpl toApplySolutionRequestImpl( eu.melodic.models.services.process.ApplySolutionRequest req)  {
		eu.melodic.models.interfaces.adapter.ApplySolutionRequestImpl interfaceRequest= new eu.melodic.models.interfaces.adapter.ApplySolutionRequestImpl();
		interfaceRequest.setApplicationId(req.getApplicationId());
		interfaceRequest.setCdoModelsPath(req.getCdoResourcePath());
		interfaceRequest.setNotificationURI("/api/adapter/applySolutionNotification/" + req.getProcessId() +"/" + req.getNotificationSubject());
		interfaceRequest.setWatermark(req.getWatermark());

		return interfaceRequest;
	}

}
