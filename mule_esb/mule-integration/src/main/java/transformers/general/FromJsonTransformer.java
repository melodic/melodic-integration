package transformers.general;

import java.io.IOException;

import javax.inject.Inject;

import org.codehaus.jackson.map.SerializationConfig;
import org.mule.api.annotations.*;
import org.mule.api.annotations.param.Payload;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;


public class FromJsonTransformer {
	
	private static final Logger logger =  LoggerFactory.getLogger(FromJsonTransformer.class);
	@Inject
	ObjectMapper objMapper;
	
	public eu.melodic.models.services.process.ConstraintProblemRequestImpl convertToCP (@Payload Object payload) throws JsonParseException, JsonMappingException, IOException{
		logger.info("inside from json transformer 1");
		return (eu.melodic.models.services.process.ConstraintProblemRequestImpl)objMapper.readValue(payload.toString(), eu.melodic.models.services.process.ConstraintProblemRequestImpl.class);
	
	}
	

}
