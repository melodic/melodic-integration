package transformers.general;

import java.io.IOException;


import org.mule.api.annotations.*;
import org.mule.api.annotations.param.Payload;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;


public class ToJsonTransformer extends ObjectMapper {
	
	//private static final Logger logger =  LoggerFactory.getLogger(ToJsonTransformer.class);
	
	 public ToJsonTransformer() {

	    }
	 public String writeValueAsString(@Payload Object object) throws JsonProcessingException {
		 return super.writeValueAsString(object);
	 }
}
