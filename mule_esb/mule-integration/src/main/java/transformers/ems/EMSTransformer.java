package transformers.ems;

import java.util.Map;

import org.mule.api.annotations.*;
import org.mule.api.annotations.param.InboundHeaders;
import org.mule.api.annotations.param.OutboundHeaders;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

@ContainsTransformerMethods // since Mule 3.0.1
public class EMSTransformer {
	
//	private static final Logger logger =  LoggerFactory.getLogger(CPSolverTransformer.class);
	
	@Transformer
	public eu.melodic.models.interfaces.ems.CamelModelRequestImpl toCamelModelRequestImpl( eu.melodic.models.services.process.EventModelRequestImpl req, @OutboundHeaders
			Map<String, Object> outboundHeaders)  {
		eu.melodic.models.interfaces.ems.CamelModelRequestImpl interfaceRequest= new eu.melodic.models.interfaces.ems.CamelModelRequestImpl();
		interfaceRequest.setApplicationId(req.getApplicationId());
		interfaceRequest.setNotificationURI("/api/ems/camelModelNotification/" + req.getProcessId() +"/" + req.getNotificationSubject());
		interfaceRequest.setWatermark(req.getWatermark());
		return interfaceRequest;
	}

	@Transformer
	public eu.melodic.models.interfaces.ems.MonitorsDataRequestImpl toMonitorsDataRequestImpl( eu.melodic.models.services.adapter.MonitorsDataRequestImpl req, @OutboundHeaders
			Map<String, Object> outboundHeaders)  {
		eu.melodic.models.interfaces.ems.MonitorsDataRequestImpl interfaceRequest= new eu.melodic.models.interfaces.ems.MonitorsDataRequestImpl();
		interfaceRequest.setApplicationId(req.getApplicationId());
		interfaceRequest.setWatermark(req.getWatermark());
		return interfaceRequest;
	}

}

