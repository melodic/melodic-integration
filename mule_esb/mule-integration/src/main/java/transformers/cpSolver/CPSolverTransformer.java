package transformers.cpSolver;

import java.util.Map;

import org.mule.api.annotations.*;
import org.mule.api.annotations.param.InboundHeaders;
import org.mule.api.annotations.param.OutboundHeaders;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

@ContainsTransformerMethods // since Mule 3.0.1
public class CPSolverTransformer {
	
//	private static final Logger logger =  LoggerFactory.getLogger(CPSolverTransformer.class);
	
	@Transformer
	public eu.melodic.models.interfaces.cpSolver.ConstraintProblemSolutionRequestImpl toConstraintProblemRequestImpl( eu.melodic.models.services.process.SolutionRequestImpl req, @OutboundHeaders
			Map<String, Object> outboundHeaders)  {
		eu.melodic.models.interfaces.cpSolver.ConstraintProblemSolutionRequestImpl interfaceRequest= new eu.melodic.models.interfaces.cpSolver.ConstraintProblemSolutionRequestImpl();
		interfaceRequest.setApplicationId(req.getApplicationId());
		interfaceRequest.setCdoModelsPath(req.getCdoResourcePath());
		interfaceRequest.setNotificationURI("/api/cpSolver/solutionNotification/" + req.getProcessId() +"/" + req.getNotificationSubject());
		interfaceRequest.setWatermark(req.getWatermark());
		interfaceRequest.setTimeLimit(120); //2 minutes
		return interfaceRequest;
	}

}

