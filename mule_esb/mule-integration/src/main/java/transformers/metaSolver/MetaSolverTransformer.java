package transformers.metaSolver;

import java.util.Map;

import org.mule.api.annotations.*;
import org.mule.api.annotations.param.InboundHeaders;
import org.mule.api.annotations.param.OutboundHeaders;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

@ContainsTransformerMethods // since Mule 3.0.1
public class MetaSolverTransformer {
	
//	private static final Logger logger =  LoggerFactory.getLogger(CPSolverTransformer.class);
	
	@Transformer
	public eu.melodic.models.interfaces.metaSolver.ConstraintProblemEnhancementRequestImpl toConstraintProblemEnhancementRequestImpl( eu.melodic.models.services.process.ConstraintProblemEnhancementRequestImpl req, @OutboundHeaders
			Map<String, Object> outboundHeaders)  {
		eu.melodic.models.interfaces.metaSolver.ConstraintProblemEnhancementRequestImpl interfaceRequest= new eu.melodic.models.interfaces.metaSolver.ConstraintProblemEnhancementRequestImpl();
		interfaceRequest.setApplicationId(req.getApplicationId());
		interfaceRequest.setCdoModelsPath(req.getCdoModelsPath());
		interfaceRequest.setWatermark(req.getWatermark());
		
		return interfaceRequest;			
	}
	
	@Transformer
	public eu.melodic.models.interfaces.metaSolver.SolutionEvaluationRequestImpl toSolutionEvaluationRequestImpl( eu.melodic.models.services.process.SolutionEvaluationRequestImpl req, @OutboundHeaders
			Map<String, Object> outboundHeaders)  {
		eu.melodic.models.interfaces.metaSolver.SolutionEvaluationRequestImpl interfaceRequest= new eu.melodic.models.interfaces.metaSolver.SolutionEvaluationRequestImpl();
		interfaceRequest.setApplicationId(req.getApplicationId());
		interfaceRequest.setCdoModelsPath(req.getCdoModelsPath());
		interfaceRequest.setWatermark(req.getWatermark());
		
		return interfaceRequest;			
	}
	
	@Transformer
	public eu.melodic.models.interfaces.metaSolver.UpdateSolutionRequestImpl toUpdateSolutionRequestImpl( eu.melodic.models.services.process.SolutionUpdateRequestImpl req, @OutboundHeaders
			Map<String, Object> outboundHeaders)  {
		eu.melodic.models.interfaces.metaSolver.UpdateSolutionRequestImpl interfaceRequest= new eu.melodic.models.interfaces.metaSolver.UpdateSolutionRequestImpl();
		interfaceRequest.setApplicationId(req.getApplicationId());
		interfaceRequest.setCdoModelsPath(req.getCdoModelsPath());
		interfaceRequest.setDeploymentResult(req.getDeploymentResult());
		interfaceRequest.setWatermark(req.getWatermark());
		
		return interfaceRequest;			
	}

}

