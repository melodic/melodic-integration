package transformers.dlms;

import java.util.Map;

import org.mule.api.annotations.*;
import org.mule.api.annotations.param.InboundHeaders;
import org.mule.api.annotations.param.OutboundHeaders;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

@ContainsTransformerMethods // since Mule 3.0.1
public class DLMSTransformer {
	
//	private static final Logger logger =  LoggerFactory.getLogger(CPSolverTransformer.class);
	
	@Transformer
	public eu.melodic.models.interfaces.dlms.DataModelRequestImpl toDataModelRequestImpl( eu.melodic.models.services.process.DataModelRequestImpl req, @OutboundHeaders
			Map<String, Object> outboundHeaders)  {
		eu.melodic.models.interfaces.dlms.DataModelRequestImpl interfaceRequest= new eu.melodic.models.interfaces.dlms.DataModelRequestImpl();
		interfaceRequest.setApplicationId(req.getApplicationId());
		interfaceRequest.setNotificationURI("/api/dlms/dataModelNotification/" + req.getProcessId() +"/" + req.getNotificationSubject());
		interfaceRequest.setWatermark(req.getWatermark());
		return interfaceRequest;
	}

}

