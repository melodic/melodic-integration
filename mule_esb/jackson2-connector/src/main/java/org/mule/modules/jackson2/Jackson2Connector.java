package org.mule.modules.jackson2;

import java.io.IOException;

import javax.inject.Inject;

import org.mule.api.MuleContext;
import org.mule.api.annotations.Config;
import org.mule.api.annotations.Connector;
import org.mule.api.annotations.Processor;
import org.mule.api.annotations.param.Payload;
import org.mule.modules.jackson2.config.ConnectorConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import eu.melodic.models.services.process.*;


@Connector(name="jackson2", friendlyName="Jackson2")
public class Jackson2Connector {
	
	private static final Logger logger =  LoggerFactory.getLogger(Jackson2Connector.class);

    private static final String JACKSON2_OBJ_MAPPER = "jackson2ObjMapper";

    @Config
    ConnectorConfig config;
    
    @Inject 
    MuleContext muleContext;
    
    ObjectMapper objMapper;
    

	
    @Processor
    public Object fromJson(@Payload String payload) throws JsonParseException, JsonMappingException, ClassNotFoundException, IOException {
        /*
         * MESSAGE PROCESSOR CODE GOES HERE
         */
    	this.registerObjectMapper();
    	
    	return objMapper.readValue(payload.toString(), Class.forName(config.getReturnClass()));
    }
    
    @Processor
    public String writeValueAsString(@Payload Object object) throws JsonProcessingException {
    	
    	this.registerObjectMapper();
    	
		return objMapper.writeValueAsString(object);
	 }

    public ConnectorConfig getConfig() {
        return config;
    }

    public void setConfig(ConnectorConfig config) {
        this.config = config;
    }
    
    public MuleContext getMuleContext() {
        return muleContext;
    }

    public void setMuleContext(MuleContext muleContext) {
        this.muleContext = muleContext;
    }
    
	public ObjectMapper getObjectMapper() {	
		
		return objMapper;
	}

	public void setObjectMapper(ObjectMapper objectMapper) {
		this.objMapper = objectMapper;
	}
	
	public ObjectMapper registerObjectMapper() {
		
		if (objMapper ==null) {
			objMapper = muleContext.getRegistry().lookupObject(JACKSON2_OBJ_MAPPER);
			if (objMapper ==null) {
				
				logger.debug("Creating new jackson2 object mapper inside MULE Registry.");
				try {
					muleContext.getRegistry().registerObject(JACKSON2_OBJ_MAPPER, new ObjectMapper());
				} catch (Exception e) {
		    		System.out.println("Mapper NOT registered!");
		    		e.printStackTrace();
		    	}
				
				//logger.info("Jackson2 objMapper: " + JACKSON2_OBJ_MAPPER + " created.");
				
				objMapper = muleContext.getRegistry().lookupObject(JACKSON2_OBJ_MAPPER);
			}
			
		} else {
			logger.debug("Using existing jackson2 obj mapper.");
		}
		
		return objMapper;
	}
	
}