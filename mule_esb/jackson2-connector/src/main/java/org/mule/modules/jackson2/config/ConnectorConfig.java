package org.mule.modules.jackson2.config;

import org.mule.api.annotations.components.Configuration;
import org.mule.api.annotations.Configurable;
import org.mule.api.annotations.param.Default;

@Configuration(friendlyName = "Configuration")
public class ConnectorConfig {

    /**
     * Greeting message
     */
    @Configurable
    @Default("java.lang.HashMap")
    private String returnClass;

    /**
     * Set greeting message
     *
     * @param greeting the greeting message
     */
    public void setReturnClass(String greeting) {
        this.returnClass = greeting;
    }

    /**
     * Get greeting message
     */
    public String getReturnClass() {
        return this.returnClass;
    }

   

}