/*
 * Copyright 2013-2017 (c) MuleSoft, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific
 * language governing permissions and limitations under the License.
 */
package org.raml.jaxrs.codegen.maven;

import org.apache.commons.io.FileUtils;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import org.codehaus.plexus.util.StringUtils;
import org.jsonschema2pojo.AnnotationStyle;
import org.raml.jaxrs.generator.Configuration;
import org.raml.jaxrs.generator.RamlScanner;
import org.raml.jaxrs.generator.builders.extensions.resources.TrialResourceClassExtension;
import org.raml.jaxrs.generator.extension.resources.GlobalResourceExtension;
import org.raml.jaxrs.generator.extension.types.LegacyTypeExtension;
import org.raml.jaxrs.generator.extension.types.TypeExtension;

import java.io.*;
import java.nio.file.Path;
import java.util.*;

import static org.apache.maven.plugins.annotations.ResolutionScope.COMPILE_PLUS_RUNTIME;

@Mojo(name = "generate", requiresProject = true, threadSafe = false, requiresDependencyResolution = COMPILE_PLUS_RUNTIME,
    defaultPhase = LifecyclePhase.GENERATE_SOURCES)
public class RamlJaxrsCodegenMojo extends AbstractMojo {

  @Parameter(defaultValue = "${project}")
  private MavenProject project;

  /**
   * Skip plug-in execution.
   */
  @Parameter(property = "skip", defaultValue = "false")
  private boolean skip;

  /**
   * Target directory for generated Java source files.
   */
  @Parameter(property = "outputDirectory", defaultValue = "${project.build.directory}/generated-sources/raml-jaxrs")
  private File outputDirectory;

  /**
   * An array of locations of the RAML file(s).
   */
  @Parameter(property = "ramlFile", required = false)
  private File ramlFile;

  /**
   * Directory location of the RAML file(s).
   */
  @Parameter(property = "sourceDirectory", defaultValue = "${basedir}/src/main/raml")
  private File sourceDirectory;

  /**
   * Resource package name used for generated Java classes.
   */
  @Parameter(property = "resourcePackage", required = true)
  private String resourcePackage;

  /**
   * Model package name used for generated Java classes.
   */
  @Parameter(property = "modelPackage")
  private String modelPackage;

  /**
   * Model package name used for generated Java classes. Models will be placed in the package:
   */
  @Parameter(property = "supportPackage")
  private String supportPackage;


  /**
   * Whether to empty the output directory before generation occurs, to clear out all source files that have been generated
   * previously.
   */
  @Parameter(property = "removeOldOutput", defaultValue = "false")
  private boolean removeOldOutput;


  /**
   * Annotate raml 1.0 types with either jackson, jaxb or gson
   */
  @Parameter(property = "generateTypesWith")
  private String[] generateTypesWith;

  /**
   * The JSON object mapper to generate annotations to: either "jackson1", "jackson2" or "gson" or "none"
   */
  @Parameter(property = "jsonMapper", defaultValue = "jackson2")
  private String jsonMapper;

  /**
   * Optional extra configuration provided to the JSON mapper. Supported keys are: "generateBuilders", "includeHashcodeAndEquals",
   * "includeToString", "useLongIntegers"
   */
  @Parameter(property = "jsonMapperConfiguration")
  private Map<String, String> jsonMapperConfiguration;

  /**
   * The name of a generator extension class (implements org.raml.jaxrs.generator.extension.resources.GlobalResourceExtension)
   */
  @Parameter(property = "resourceCreationExtension")
  private String resourceCreationExtension;

  /**
   * The name of a generator extension class (implements org.raml.jaxrs.generator.extension.resources.GlobalResourceExtension)
   */
  @Parameter(property = "resourceCreationExtension")
  private String resourceFinishExtension;

  /**
   * The name of a generator extension class (implements org.raml.jaxrs.codegen.core.ext.GeneratorExtension)
   */
  @Parameter(property = "typeExtensions")
  private String[] typeExtensions;


  @Override
  public void execute() throws MojoExecutionException, MojoFailureException {
    if (skip) {
      getLog().info("Skipping execution...");
      return;
    }

    try {
      FileUtils.forceMkdir(outputDirectory);
    } catch (final IOException ioe) {
      throw new MojoExecutionException("Failed to create directory: " + outputDirectory, ioe);
    }

    if (removeOldOutput) {
      try {
        FileUtils.cleanDirectory(outputDirectory);
      } catch (final IOException ioe) {
        throw new MojoExecutionException("Failed to clean directory: " + outputDirectory, ioe);
      }
    }

    // build list of raml files and configs
    HashMap<File, Configuration> configMap = new HashMap<>();

    for (File file : getRamlFiles()) {
      try {
        String line = getFirstLine(file);
        if (line.compareTo("#%RAML 1.0") == 0) {
          getLog().info("Generating Java classes from: " + file);
          final Configuration configuration = getConfiguration(file);
          configMap.put(file, configuration);
        } else {
          getLog()
              .info(file
                  + " does not seem to be RAML root file -skipped(first line should start from #%RAML ${raml version number}");
        }

      } catch (Exception e) {
        getLog()
            .info(ramlFile
                + " does not seem to be RAML root file -skipped(first line should start from #%RAML ${raml version number}");
      }
    }

    project.addCompileSourceRoot(outputDirectory.getPath());

    try {
      for (File file : configMap.keySet()) {
        new RamlScanner(configMap.get(file)).handle(file);
      }
    } catch (final Exception e) {
      throw new MojoExecutionException("Error generating Java classes from: ", e);
    }
  }

  private String getFirstLine(File file) throws IOException {
    try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
      return reader.readLine();
    }
  }

  private Collection<File> getRamlFiles() throws MojoExecutionException {
    if (!sourceDirectory.isDirectory()) {
      throw new MojoExecutionException("The provided path doesn't refer to a valid directory: "
          + sourceDirectory);
    }

    getLog().info("Looking for RAML files in and below: " + sourceDirectory);

    return FileUtils.listFiles(sourceDirectory, new String[] {"raml", "yaml"}, true);
  }

  private Configuration getConfiguration(File singleRamlFile) throws MojoExecutionException {
    final Configuration configuration = new Configuration();

    try {

      configuration.setModelPackage(modelPackage.concat(computeSubPackageName(singleRamlFile)));
      configuration.setResourcePackage(resourcePackage.concat(computeSubPackageName(singleRamlFile)));
      configuration.setSupportPackage(supportPackage);
      configuration.setOutputDirectory(outputDirectory);
      configuration.setJsonMapper(AnnotationStyle.valueOf(jsonMapper.toUpperCase()));
      configuration.setJsonMapperConfiguration(jsonMapperConfiguration);
      configuration.setTypeConfiguration(generateTypesWith);
      if (resourceCreationExtension != null) {

        Class<GlobalResourceExtension> c = (Class<GlobalResourceExtension>) Class.forName(resourceCreationExtension);
        configuration.defaultResourceCreationExtension(c);

      }

      if (resourceFinishExtension != null) {

        Class<GlobalResourceExtension> c = (Class<GlobalResourceExtension>) Class.forName(resourceCreationExtension);
        configuration.defaultResourceFinishExtension(c);
      }

      if (typeExtensions != null) {
        for (String className : typeExtensions) {
          Class c = Class.forName(className);
          if (c == null) {
            throw new MojoExecutionException("typeExtension " + className
                + " cannot be loaded."
                + "Have you installed the correct dependency in the plugin configuration?");
          }
          if (!((c.newInstance()) instanceof TypeExtension)) {
            throw new MojoExecutionException("typeExtension " + className
                + " does not implement" + TrialResourceClassExtension.class.getPackage() + "."
                + TrialResourceClassExtension.class.getName());

          }

          configuration.getTypeExtensions().add((LegacyTypeExtension) c.newInstance());
        }
      }

    } catch (final Exception e) {
      throw new MojoExecutionException("Failed to configure plug-in", e);
    }
    return configuration;
  }

  /**
   * If useSourceHierarchy is true and sourceDirectory is set, return a sub package name that reflects the relative position of
   * the ramlFile to sourceDirectory. Otherwise return an empty string.
   *
   * @param ramlFile
   * @return computed sub package name
   */
  protected String computeSubPackageName(File ramlFile) {
    String subName = "";
    Path rel = sourceDirectory.toPath().relativize(ramlFile.toPath());
    if (rel != null && rel.getParent() != null) {
      String parent = rel.getParent().toString();
      String rpn = parent.replace(File.separator, ".");
      if (StringUtils.isNotBlank(rpn)) {
        subName = ".".concat(rpn);
      }
    }
    return subName;
  }


}
